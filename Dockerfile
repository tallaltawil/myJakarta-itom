FROM    centos:centos7
MAINTAINER The IdeaLab Project <IdeaLab@bah.com>
LABEL version="release-0.0.1" description="IdeaLab Docker ServiceNow Container"

# Declare the default shell
SHELL ["/bin/bash", "-c"] 

# Default to UTF-8 file.encoding
ENV LC_ALL en_US.UTF-8

# Declare Build Args
ARG host
ARG username
ARG pass
ENV host ${host}
ENV username ${username}
ENV pass ${pass}

#  Install Yum Repo Tools
RUN \
  yum update -y && \
  yum install -y --nogpgcheck tar util-linux wget zip unzip gcc which vim curl nano zsh

# Install Tools and Supervisor
RUN \
  echo sslverify=false >> /etc/yum.conf && \
  yum install -y --nogpgcheck epel-release && \
  yum install -y python-pip htop && \
  yum clean all && \
  easy_install supervisor && \
  pip install supervisor-stdout

# Install JDK 8
RUN \
  yum install -y --nogpgcheck java-1.8*

# Configure Supervisor
RUN \
  mkdir -p /etc/supervisor/conf.d && \
  /usr/bin/echo_supervisord_conf > /etc/supervisord.conf && \
  sed -i -e "s/^nodaemon=false/nodaemon=true/" /etc/supervisord.conf && \
  echo [include] >> /etc/supervisord.conf && \
  echo 'files = /etc/supervisor/conf.d/*.conf' >> /etc/supervisord.conf
ADD docker-snow.conf /etc/supervisor/conf.d/

# Install & Configure ServiceNow
# # INSTALL AWS CLI to get ServiceNow installation file from S3 and place in /tmp

RUN yum install unzip -y
RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
RUN unzip awscli-bundle.zip
RUN ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
RUN /usr/local/bin/aws configure set aws_access_key_id AKIAJC3RDCW5ABC4NHYQ
RUN /usr/local/bin/aws configure set aws_secret_access_key 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
RUN /usr/local/bin/aws configure set default.region us-east-1
RUN /usr/local/bin/aws s3 cp s3://icite-itom/servicenow/media/mid.jakarta-05-03-2017__patch3-08-23-2017_09-05-2017_1648.linux.x86-64.zip /tmp/


# # Prep ServiceNow
# # tallal - commenting out Prep ServiceNow section; not needed for MID Server Install

#RUN export SERVICENOW_MOUNT=/glide
#RUN export SERVICENOW_JAR_ZIP_FILE=/tmp/jakarta.zip
#RUN export SERVICENOW_APP_NAME=sn_16001
#RUN export SERVICENOW_APP_LONG_NAME="Glide DockerSnow"
#RUN export SERVICENOW_INSTALL_DIR=$SERVICENOW_MOUNT/nodes/$SERVICENOW_APP_NAME
#RUN export SERVICENOW_INSTALL_TIMEOUT=20
#RUN export RDS_ENDPOINT_ADDRESS=${host}
#RUN export RDS_DB_USERNAME=${username}
#RUN export RDS_DB_PASSWORD=${pass}
#RUN export JAVA_HOME=/etc/alternatives/java_sdk
#RUN export PATH=/sbin:/bin:/usr/sbin:/usr/bin
#RUN export PORT=16001
#RUN export JAVA_HOME

# # Will add parameters for MID Server Deployment after confirming hard coded works.

# Need to add URL Parameter for config.xml file
# Need to add MID Server username
# Need to add Username Password
# Need to add MID Server Name
# Need to add ServiceNow Service now mount 
# Need to add Servicenow MID server directory name 
# Need to add install directory variable


# # MORE prep
# tallal - not required for MID server

#RUN yum -y install glibc* glibc.i686

# # Prep for ServiceNow install
# tallal - will run install initially as root

#RUN useradd servicenow

# # Install ServiceNow
#tallal - not applicable

#RUN runuser -l servicenow -c "java -jar /tmp/jakarta.zip --dst-dir /glide/nodes/sn_16001 install -n sn -p 16001"
#RUN java -jar /tmp/jakarta.zip --dst-dir /glide/nodes/sn_16001 install -n sn -p 16001

# # Prep MID Server
RUN sudo su
RUN mkdir -p /servicenow/mid-dev/
RUN unzip /tmp/mid.jakarta-05-03-2017__patch3-08-23-2017_09-05-2017_1648.linux.x86-64.zip -d /servicenow/mid-dev/
RUN cp /servicenow/mid-dev/agent/config.xml /tmp/mid-server-config.xml.backup

# # Create MID Server config file
RUN echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<parameters>
       <parameter name=\"url\" value=\"https://dev42659.service-now.com\"/>
       <parameter name=\"mid.instance.username\" value=\"service_account_mid\" />
       <parameter name=\"mid.instance.password\" value=\"MID1234!\" encrypt=\"true\"/>
       <parameter name=\"name\" value=\"conatiner-mid\"/>
       <parameter name=\"threads.max\" value=\"25\"/>
       <parameter name=\"mid_sys_id\" value=\"\"/>    
</parameters>" >> "/servicenow/mid-dev/agent/config.xml"

# # Cleanup Media
RUN rm --force /servicenow/mid-dev/agent/config.xml

# # Start the installer
RUN cd /servicenow/mid-dev/agent/
RUN ./start.sh

# # Auto start MID server on host reboot
RUN /servicenow/mid-dev/agent/bin/mid.sh install

# Remove ServiceNow demo data

#RUN export SERVICENOW_ZBOOT="${SERVICENOW_INSTALL_DIR}/webapps/glide/itil/WEB-INF/sys.scripts/zboot_cold_instance.js"
#RUN sed -i -e "/db_reset_successful/d" "/glide/nodes/sn_16001/webapps/glide/itil/WEB-INF/sys.scripts/"
#RUN sed -i -e "/zboot_demo_data.js/d" "/glide/nodes/sn_16001/webapps/glide/itil/WEB-INF/sys.scripts/"
#RUN sed -i -e '/}/d' "/glide/nodes/sn_16001/webapps/glide/itil/WEB-INF/sys.scripts/"
# Update ServiceNow DB properties file
#RUN mkdir /glide/nodes/sn_16001/conf
#RUN chown -R servicenow:servicenow /glide

# # tallal - not needed for mid server
# RUN mkdir -p /glide/nodes/sn_16001/conf
# RUN touch /glide/nodes/sn_16001/conf/glide.db.properties


# RUN export SERVICENOW_DB_PROPERTIES="${SERVICENOW_INSTALL_DIR}/conf/glide.db.properties"
# RUN echo "glide.db.user = ${username}" >> "/glide/nodes/sn_16001/conf/glide.db.properties"
# RUN echo "glide.db.password = ${pass}" >> "/glide/nodes/sn_16001/conf/glide.db.properties"
# RUN echo "glide.db.encrypt = true" >> "/glide/nodes/sn_16001/conf/glide.db.properties"
# RUN sed -i -e "s#<DatabaseAccount>#${username}#g" "/glide/nodes/sn_16001/conf/glide.db.properties"
# RUN sed -i -e "s#<Password>#${pass}#g" "/glide/nodes/sn_16001/conf/glide.db.properties"
# RUN sed -i -e "s#localhost#${host}#g" "/glide/nodes/sn_16001/conf/glide.db.properties"

# Update ServiceNow Glide properties file

# RUN export SERVICENOW_GLIDE_PROPERTIES="${SERVICENOW_INSTALL_DIR}/conf/glide.properties"
# RUN touch /glide/nodes/sn_16001/conf/glide.properties
# RUN sed -i '/16001/a glide.db.pooler.connections = 32' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.db.pooler.connections.max = 32' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.sys.schedulers = 8' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.monitor.url = docker-snow' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.self.monitor.fast_stats = false' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.self.monitor.checkin.interval = 86400000' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.self.monitor.server_stats.interval = 86400000' "/glide/nodes/sn_16001/conf/glide.properties"
# RUN sed -i '/16001/a glide.self.monitor.fast_server_stats.interval = 86400000' "/glide/nodes/sn_16001/conf/glide.properties"

# Add ServiceNow StartUp Script
# ADD startSnow.sh /glide/nodes/sn_16001/
# RUN chmod +x /glide/nodes/sn_16001/startSnow.sh

# Set JAVA_HOME
# ENV JAVA_HOME /etc/alternatives/java_sdk

# Document Exposed Ports
# EXPOSE 16001

# Default command
CMD ["/usr/bin/supervisord"]
